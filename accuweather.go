package accuweather

import (
	"strings"

	"github.com/araddon/dateparse"
	"github.com/mmcdole/gofeed"
)

const weatherStateLocation = 1
const temperatureLocation = 2
const version = "1.0.1"

func obtainFeed(tempUnit string, cityId string) (feed *gofeed.Feed, err error) {
	fp := gofeed.NewParser()
	feed, err = fp.ParseURL("http://rss.accuweather.com/rss/liveweather_rss.asp?" + tempUnit + "=1&locCode=" + cityId)
	return
}

func obtainCurrentData(tempUnit string, cityId string) (weatherState string, temperature string, icon string, err error) {
	feed, err := obtainFeed(tempUnit, cityId)
	if err != nil {
		return
	}

	title := feed.Items[0].Title
	Description := feed.Items[0].Description
	firstSplitOfBlob := strings.Split(Description, "<img src=\"")
	secondSplitOfBlob := strings.Split(firstSplitOfBlob[1], "\">")
	icon = secondSplitOfBlob[0]
	splitText := strings.Split(title, ":")

	weatherState = strings.Trim(splitText[weatherStateLocation], "\t \n")

	if tempUnit == "imperial" {
		temperature = strings.Trim(splitText[temperatureLocation], "\t \n F") + " °F"
	} else {
		temperature = strings.Trim(splitText[temperatureLocation], "\t \n C") + " °C"
	}
	return

}

func extractDayOfWeek(date string) (dayOfWeek string) {

	dateProper, err := dateparse.ParseAny(date)

	if err != nil {
		return "Invalid Date"
	}

	dayOfWeek = dateProper.Weekday().String()

	return

}

func formatWeatherState(weatherState string) (output string) {

	output = strings.Replace(weatherState, " F ", "°F\n", 2)
	output = strings.Replace(output, " C ", "°C\n", 2)

	return
}

func obtainForcast(tempUnit string, cityID string, forcast int) (weatherState string, icon string, day string, err error) {
	feed, err := obtainFeed(tempUnit, cityID)
	if err != nil {
		return
	}

	title := feed.Items[forcast].Title
	arrayFromTitle := strings.Split(title, " ")
	date := arrayFromTitle[0]

	day = extractDayOfWeek(date)

	description := feed.Items[forcast].Description
	firstPartOfDescription := strings.Split(description, "<img src=\"")
	weatherState = formatWeatherState(firstPartOfDescription[0])
	icon = strings.Trim(firstPartOfDescription[1], "\" >")
	return

}

// TextForForcast ... This function exports a forcast only with no symbol
func TextForForcast(tempUnit string, cityID string, forcast int) (output string) {
	weatherState, _, day, _ := obtainForcast(tempUnit, cityID, forcast)
	output = day + "\n" + weatherState
	return
}

// IconForForcast ... This function exports a url for an icon for forcast
func IconForForcast(cityID string, forcast int) (icon string) {
	_, icon, _, _ = obtainForcast("imperial", cityID, forcast)
	return
}

// TextCurrent ... This reports
func TextCurrent(tempUnit string, cityID string) (output string) {

	weatherState, temperature, _, _ := obtainCurrentData(tempUnit, cityID)
	output = weatherState + " " + temperature
	return
}

// IconCurrent ... The icon representing the current weather conditions
func IconCurrent(cityID string) (icon string) {
	_, _, icon, _ = obtainCurrentData("imperial", cityID)
	return
}

// TempCurrent ... This is the temp from the current weather condititions
func TempCurrent(tempUnit string, cityId string) (temp string) {
	_, temp, _, _ = obtainCurrentData(tempUnit, cityId)
	return
}
