package accuweather

import (
	"encoding/json"
	"strings"
	"testing"
)

func isJSON(s string) bool {
	var js map[string]interface{}
	return json.Unmarshal([]byte(s), &js) == nil

}
func TestExtractDayOfWeek(t *testing.T) {

	day := extractDayOfWeek("12/1/2001")

	testValue := "Saturday"

	if day != testValue {

		t.Error("This should have been " + testValue)

	}
	day = extractDayOfWeek("12/40/2001")

	testValue = "Invalid Date"

	if day != testValue {

		t.Error("This should have been " + testValue)

	}

}

func TestObtainFeed(t *testing.T) {

	feedResult, err := obtainFeed("imperial", "EN|US|DENVER")

	if !isJSON(feedResult.String()) {
		t.Error("This should be json!")
	}
	if feedResult.FeedType != "rss" {
		t.Error("This should be an rss feed.")

	}

	if err != nil {
		t.Error("This should be a successful feed and is not!")
	}

	if len(feedResult.Items) != 4 {
		t.Error("This feed should contain 4 items!")
	}
}

func TestObtainCurrentData(t *testing.T) {

	weatherState, temperature, icon, err := obtainCurrentData("imperial", "EN|US|DENVER")
	if strings.Contains(weatherState, "°F") {

		t.Error("The weather State should not contain deg F!")

	}

	if !strings.Contains(temperature, "°F") {

		t.Error("The temperature should contain deg F!")

	}
	if !strings.Contains(icon, "http") {

		t.Error("The weather State should contain deg F!")

	}

	if err != nil {
		t.Error("No error should be generated")
	}
	_, temperature, _, _ = obtainCurrentData("metric", "EN|US|DENVER")

	if !strings.Contains(temperature, "°C") {

		t.Error("The temperature should contain deg F!")

	}

}

func TestFormatWeatherState(t *testing.T) {

	testString := " F F Facts"
	returnedString := formatWeatherState(testString)

	if !strings.Contains(returnedString, "°F\n") {

		t.Error("This should contain the specified string")

	}

	testString = " C C Cacts"
	returnedString = formatWeatherState(testString)

	if !strings.Contains(returnedString, "°C\n") {

		t.Error("This should contain the specified string")

	}

	testString = "FaCts"
	returnedString = formatWeatherState(testString)
	if strings.Contains(returnedString, "°C\n") || strings.Contains(returnedString, "°F\n") {

		t.Error("This should not contain the checked strings.")

	}

}

func TestObtainForcast(t *testing.T) {

	weatherState, _, _, _ := obtainForcast("imperial", "EN|US|DENVER", 1)

	if !strings.Contains(weatherState, "°F\n") {
		t.Error("This should contain a deg F and a line feed.")
	}
}

func TestTextForForcast(t *testing.T) {

	returnedString := TextForForcast("imperial", "EN|US|DENVER", 1)

	if !strings.Contains(returnedString, "\n") {
		t.Error("This should contain a newline character")
	}
}

func TestIconForForcast(t *testing.T) {

	returnedString := IconForForcast("EN|US|DENVER", 1)

	if !strings.Contains(returnedString, "http") {
		t.Error("This should contain a link to a webpage")
	}
}

func TestTextCurrent(t *testing.T) {
	returnedString := TextCurrent("metric", "EN|US|DENVER")

	if !strings.Contains(returnedString, "°C") {
		t.Error("This should contain a link to a webpage")
	}

	if len(returnedString) < 5 {
		t.Error("This should be bigger than 5 characters")

	}
}

func TestIconCurrent(t *testing.T) {

	returnedString := IconCurrent("EN|US|DENVER")

	if !strings.Contains(returnedString, "http") {
		t.Error("This should contain a link to a webpage")
	}

	if len(returnedString) < 5 {
		t.Error("This should be bigger than 5 characters")

	}
}

func TestTempCurrent(t *testing.T) {

	returnedString := TempCurrent("imperial", "EN|US|DENVER")

	if !strings.Contains(returnedString, "°F") {
		t.Error("This should contain farenheit degrees.")
	}

	if len(returnedString) > 6 {
		t.Error("This should be bigger than 5 characters")

	}
}
