This is a simple library to allow obtaining current weather conditions including a daily forcast
and a forcast for the next day (tommorow).   The publicly allowable functions are in the go
documentation.   Use as you see fit.   In truth I just wrote this as a library for my program
called "weatherreport" so that I could easily unit test it.   I also thought that perhaps
it might prove useful to someone so I released it GPLV3.

The program can also produce a forcast for the current day, or for the following day. 
In order to get a forcast for the current day set the forcast variable to 1 to get the current
conditions set it to 0 and for the next day set the variable to 2.   If the forcast is set to
anything other than 0 then using the "temp" option will still print the full text output.


Note that this software is licensed as GPL 3 but Accuweather only permits the use of their 
rss feed for "personal use" that means that this cannot be used for commercial projects.   That 
is part of the reason I licensed mine as GPL 3.    If you modify this code or use it at all
you must license it GPL 3...this effectively rules out non-personal use because most corporations
will not want this restriction on their software.   In any case respect Accuweather's requirements
I am using this for personal purpose which is mostly just to display weather info in my conky
every 5 seconds.   
