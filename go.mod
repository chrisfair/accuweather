module gitlab.com/chrisfair/accuweather

go 1.16

require (
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
	github.com/mmcdole/gofeed v1.1.3
)
